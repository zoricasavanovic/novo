\section{Typing language}

In this section we give the formal presentation of our type system for the components, that serves for the purpose of making the communication among them safe and progress. First, we start by describing the notations that we use for the type semantics in order to introduce each construct's meaning. Second, we move to the operational semantics, where we try to capture the behaviour of the components.





\subsection{Syntax of Typing language}

In the Table \ref{tab:typesyntax} the type $T$ is defined by a set of input ports $\tilde{x}$ and a list of constraints $\C$. Set of input ports is ranged over $x,z,u,v$ like in the language for GC and can be the empty set. The list of constraints $\C$ is used for describing the capabilities of the output ports. It is a (possibly empty) list of triples of the form $y:\B:[\D]$ ( for each output port, ranged over $y,u,v$). Every output port has bounded or unbounded capability for output the values. This capability $\B$ announces the maximum number of values that can be ``fired'' from the the port. With $\D$ we denote a list of dependencies that is found in two kinds of forms: $x:N$ (``pre each value'') and $x:\ic$ (``initial'') . It describes the dependencies of output port on input ports. The ``per each value'' dependency announces that per each value that the specific port wants to output it needs a value to be received on that input port. E.g. dependency of the constraint $y:\B:[x:N]$ we read as: For each value that $y$ wants to output it needs a value to be received on the port $x$, where $N$ declares the number of values received on $x$ waiting to be output on $y$. Instead, ``initial'' dependency, or we also called ``initial condition'', is a kind of dependency where $y$ needs only one value to be received on (e.g. the port $x$) after which it can output as many values as it is able (boundary).  








\vspace{0.5cm}
\begin{table}[H]
\centering
\begin{tabular}{l l}
   

Types  & $T::=\{\tilde{x}; \C\}$ 
\vspace{0.2cm}\\
Constraints & $\C::=y:\B:[\D] \ |\ \emptyset \ |\ \C_1,\C_2$
\vspace{0.2cm}\\

Dependencies & $\D::=x:M  \ |\ \emptyset \ |\ \D_1,\D_2$
\vspace{0.2cm}\\


Kinds of dependency & $M::=N\ |\ \ic$
\vspace{0.2cm}\\

Boundary & $\B::=N\ |\ \infty$
\vspace{0.2cm}\\

& $N\in \mathbb{N}_0$
\vspace{0.2cm}\\


& $x:N$-`per each value'
\vspace{0.2cm}\\

& $x:\ic$-`initial condition'
\\
\\


\end{tabular}


 \caption{Type Syntax}
    \label{tab:typesyntax}
\end{table}


\begin{convention}\thlabel{sin1} For every $\D$ and every $\C$ the following holds:

\begin{itemize}
\item {[Shuffle $\D$]}: $\D_1,\D_2\equiv\D_2,\D_1$
\item {[Shuffle $\C$]}: $\C_1,\C_2\equiv\C_2,\C_1$
\item {[Empty Concatenation $\D$]}:
$\D,\emptyset\equiv\emptyset,\D\equiv\D$
\item {[Empty Concatenation $\C$]}: $\C,\emptyset\equiv\emptyset,\C\equiv\C$
\end{itemize}
\end{convention}




\subsection{Type semantics}
\vspace{0.5cm}

Now we move to the operational semantics of the type language in terms of a labelled transition system (LTS). With $\xrightarrow{\text{$\lambda$}}$ we denote a three kinds of transitions where the syntax of labels is: $\lambda::=x?|y!|\tau$. Label $x?$ denotes an input on the port $x$, $y!$ denotes an output on the port $y$ and $\tau$ is an internal move. The rules defining the semantics of the types are separated in two tables: one for input  (displayed in Table \ref{tab:input_semantics}) and the other one for output and internal move (Table \ref{tab:output_semantics}).









\begin{table}[H]


\begin{center}


$\begin{tabular}{c c}

 \infer [{[T1]}]{\C_1,\C_2\xrightarrow{\text{x?}}\C_1',\C_2'}{\C_1\xrightarrow{\text{x?}}\C_1' & \C_2\xrightarrow{\text{x?}}\C_2'} & \infer[{[T2]}]{\{\tilde{x};\C\}\xrightarrow{\text{x?}}\{\tilde{x};\C'\}}{\C\xrightarrow{\text{x?}}\C' & x\in\tilde{x}} \\ 
& \\

\infer [{[T3]}]{\{\tilde{x};\emptyset\}\xrightarrow{\text{x?}}\{\tilde{x};\emptyset\}}{x\in\tilde{x}} &
 \infer[{[T4]}]{y:\B:[\D]\xrightarrow{\text{x?}}y:\B:[\D]}{x\notin dom[\D]}\\
 
& \\
 \infer[{[T5]}]{y:\B:[x:\ic,\D]\xrightarrow{\text{x?}}y:\B:[\D]}{} &
 \infer[[{T6]}]{y:\B:[x:N,\D]\xrightarrow{\text{x?}}y:\B:[x:N+1,\D]}{}\\

& \\





 \end{tabular}$



\end{center}
\caption {Input-Type Semantics} \label{tab:input_semantics}

\end{table}

Rule [T2] states that if the constraint can receive a value on the port $x$ on one of the input ports described in the type, then the type can input a value on the corresponding port. Rule [T3] declares that even if the list of the constraints is empty, we can always receive on the input port from the type.   Rule [T1] ([T7]) states that whenever we can receive (output) a value on (from) an input (output) port, all the constraints (dependencies) are allowed
to react to it. Rule [T4] states that if we input on the port that is not in the domain of the list of the dependencies of the constraint, then the constraint does not react to it. If in the list of the dependencies we have the output port that depends initially on some input port, then Rule [T5] states that after an input on that port, the dependency is dropped. Instead, if it was ``per each value'' dependency on the port $x$, with an input on it the number of values received for $y$ to be output, increases by one. Rule [T8] states that if the list of dependencies is able to output (it is satisfied), and the port $y$ still has ability to output, then the type can output on the port $y$. Rule [T9] states that if the port $y$ does not depend on any port, the type can always output on the corresponding port (up to the number of values allowed by the boundary ). The rule [T10] states that with an output ``per each value'' dependency will decrease the number of values received by one, as long as that number was greater than 0. Notice that if the port can output a value, the only dependencies that can have are  ``per each value'' dependencies.
















\vspace{1cm}
\begin{table}[H]

\begin{center}
    
$\begin{tabular}{c c}

 \infer [{[T7]}]{\D_1,\D_2\xrightarrow{\text{!}}\D_1',\D_2'}{\D_1\xrightarrow{\text{!}}\D_1' & \D_2\xrightarrow{\text{!}}\D_2'} &
 
 \infer[{[T8]}]{\{\tilde{x};y:\B:[\D],\C'\}\xrightarrow{\text{y!}}\{\tilde{x};y:\B-1:[\D'],\C'\}}{\D\xrightarrow{\text{!}}\D' & \B>0}\\
 
& \\

\infer [{[T9]}]{\{\tilde{x};y:\B:[\emptyset],\C\}\xrightarrow{\text{y!}}\{\tilde{x};y:\B-1:[\emptyset],\C\}}{\B>0} &
 \infer[{[T10]}]{x:N\xrightarrow{\text{!}}x:N-1}{ N\geq 1}\\



& \\
\multicolumn{2}{c}{\infer[{[T11]}]{T\xrightarrow{\text{$\lambda$}}T}{}}\\
& \\


 \end{tabular}$



\end{center}
\caption {Output/Internal-Type Semantics} \label{tab:output_semantics}
\end{table}















    
  \textbf{Local protocol}\\
  
  Local protocol $LP$ is a projection of a global protocol, or a protocol of a composite component to an internal component. The syntax of a local protocol is given in the Table \ref{tab:localprotocol}. The term $x?.LP$ denotes that a component has an input port $x$ and that only after an input on it, continues as a protocol $LP$. The term $y$ is similar, but the inner component has to output first. Then we have a standard recursion and the termination of the protocol.
  
  
  
  
  
  \vspace{0.5cm}
  \begin{table}[H]
  \begin{center}
  \begin{tabular}{r l}
      $LP:=$  & $\ \  x?.LP$ \\
       & $|\ y!.LP$\\
       & $|\ recX.LP$\\
       & $|\ X$ \\
       & $|\ end$\\
       & \\
       
  \end{tabular}
    \end{center}
\caption {Local Protocol} \label{tab:localprotocol}
\end{table}



