\documentclass[12pt]{spieman} 





\usepackage{stackengine}
\usepackage{mathtools}


\usepackage{float}
\usepackage{color, colortbl}
\usepackage[margin=1in]{geometry} 
\usepackage{amsthm}
\usepackage{amsmath,amssymb}
\usepackage{cleveref}
\crefformat{table}{Table}
\usepackage{tocloft}
\usepackage{proof}
\usepackage{theoremref}
\usepackage{enumitem}





\input{macros.tex}

\title{Behavioural types for reactive components}


\author{Zorica Savanovi\'c\\
IMT School for Advanced Studies Lucca, SysMA research unit\\ Supervisors: Asst. Prof. Hugo Torres Vieira and Letterio Galletta}





\renewcommand{\cftdotsep}{\cftnodots}
\cftpagenumbersoff{figure}
\cftpagenumbersoff{table} 
\begin{document} 
\maketitle

\begin{abstract}

Building complex systems from the assembly of simpler components, including support for reusability and substitution, is a mainstream approach for software development. In a distributed environment where components exchange messages, communication among the assembly of components is expected  to follow certain predefined protocols. Our research is focused on describing the behaviour of these components, given in terms of reactive data ports. The component can be used in environments that expect a particular sequence of input/output actions performed by it, but this interferes with reusability. Instead, if the components are ``reactive", in the sense that are triggered by external stimulus, reusability is promoted, but the components can be ``too wild'". We propose the approach where the components that are used in compositions under some protocol and the reactions supported by the component are enough to implement their part in the protocols. The goal  is to develop typing descriptions that allow to verify that components provide the reactive behaviour required by the context. Once the component's type is identified, there is no further need to check the source code, so each time the component is used it suffices to consider the type to ensure the required behaviour is provided.

\end{abstract}

\keywords{types, CBD, distributed systems, software engineering, reusability} 


\begin{spacing}{1.5}   % use double spacing for rest of manuscript

\section{Introduction}

The size and complexity of software are increasing rapidly. To develop software systems with a low cost and in a short period of time, code reusability has become an important requirement in the software development industry today. Component-Based Development (CBD) or Component-Based Software Engineering (CBSE) emerged from a
failure  of top-down development approaches that use problem decomposition methods, to support effective software reuse. This is because code is too application specific, while, instead, components  are more general-purpose.
 
The CBD idea was envisioned more than forty years ago \cite{Mcllroy}. However, most of the research work on CBD has been developed more recently. The motivations behind CBD were increasing efficiency and lower costs (reduces production cost by building a system from pre-existing components, instead from scratch) on the one side, and improving quality with regards to fewer errors (we can test components over and over again in a different systems) on the other side. One of the major advantages using component-based approaches is to support reuse. For example, microservices that are the new rising architecture in the enterprise market, and mainly adopted by massively deployed applications such as Netflix, eBay, Amazon
and even Uber, are reusable services that communicate through message passing. The component-based approach adapts well the paradigm of microservices. 
 



In an environment where the components exchange messages, communication among them is expected to follow certain predefined protocols. Protocols can be defined in terms of some choreography language like, for example, WS-CDL \cite{WS}. A component must implement a certain sequence of input/output actions in order to implement its role in a protocol. This interferes with reusability, due to the fact that the component can be used only in an environment that expects a particular sequence of input/output actions performed by it. For instance,  if we design a protocol where a component receives a number and after, due to the protocol, it must output the half of its value, what will happen if we need to use this component in a context that requires computing its half more than once?

The solution can be in reactive programming, where ``reactive" means triggered by external stimulus. For example, we design a component that after receiving a number, it is able to output half of its value as long as required. This way, reusability is promoted: one component can be used in a different environments. However, reactive components may behave in an unexpected way and we might not be able to define clearly the notion of the protocol. What if, for example, we have resource boundaries, and we can only output two or three times after receiving a value? Carbone, Montesi and Vieira \cite{Hugo} proposed a language that supports the development of distributed systems, where the components are assembled together with a protocol that governs the flow of communication among the components. This approach merges reactive programming with choreographic specifications of communication protocols, and it is the background for the work presented in this paper.

As in \cite{Hugo} we consider that components can dynamically produce results as soon as all the inputs are received. We also consider that an assembly of components is governed by a protocol, and that this composition is a component itself. These components may be used in a composition governed by some protocol as long as the reactions supported by the component are enough to implement its part in the protocol. This principle takes reusability to a different level: abstraction from the number of reactions (a) and abstraction from message ordering (b).
\begin{enumerate}[label=\alph*)]
   
  
    
    \item if a component can perform a computation in reaction to some data inputs, than it can be used in a different protocols that require such computation a different number of times.
        \item if a component needs some values to perform a computation, it can be used with any protocol that provides them in any order. 
        
    \end{enumerate}

Component implementations should be hidden, in the sense that the interface of the component is the component's signature-we should not need to know about the inner workings. For the purpose of \textit{off-the-shelf} substitution or reuse, it is important to ensure that each component provides (at least) the reactive behaviour as needed by the protocol that it participates in.

\textit{Can you do this?}

The typing discipline proposed in \cite{Hugo} addresses communication safety and progress of system. The approach requires checking the reactive behaviour of components each time the component is put in a different context i.e., each time that the component is used ``off-the-shelf" we need to check if the reactions supported by the component are enough to implement its part in the protocol. 

\textit{What can you do?}

 The main goal of this research is: once we identify the type of the component, we should be able to verify that the component provides (or not) the reactive behaviour required by any context. This approach considers components as an \textit{encapsulated} entities, in the sense that, once the component's type is identified, there is no further need to check the reactions supported by the component, because its type should be enough to describe ``what the component can do".


\input{background.tex}

\input{informal_type_language.tex}

\input{syntax_and_semantics.tex}

\input{base_component.tex}

\input{type_properties.tex}

\input{extracted_composite.tex}

\input{modified_type.tex}

\input{subject_reduction.tex}

\input{progress.tex}

\end{spacing}

\bibliographystyle{abbrv}
\bibliography{bib}

\end{document}